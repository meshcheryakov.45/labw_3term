#include "headers/solve.h"
#include <cmath>
#include <stdexcept>
#include <iostream>

double* solve(double a,double b,double c){
    if(fabs(a)<eps)
        throw std::invalid_argument("a");
        
    double* result = new double[2];
    double d = b*b - 4*a*c;
    if( d >= 0 ){
        result[0] = ( -b - sqrt(d) ) / (2*a);
        result[1] = ( -b + sqrt(d) ) / (2*a);
    } else
        result = nullptr;
    return result;
}